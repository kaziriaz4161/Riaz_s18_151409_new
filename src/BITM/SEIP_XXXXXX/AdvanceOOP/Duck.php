<?php
/**
 * Created by PhpStorm.
 * User: Trainer 402
 * Date: 1/28/2017
 * Time: 3:34 PM
 */

namespace OOP;


class Duck extends Bird implements canFly,canSwim{


    public $name = "Duck";

    public function fly()
    {
        echo "I can Fly<br>";

    }

    public function swim()
    {
        echo "I can Swim<br>";
    }


}

